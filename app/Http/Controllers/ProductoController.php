<?php

namespace App\Http\Controllers;
use\App\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductoController extends Controller
{
    public function index()
    {
       return view('producto.index')->with([
        'productos' =>Producto::all(),
       ]);
    }

    public function create()
    {
        //return 'This is the form to create a product from CONTROLLER';
       return view('producto.create');
    }

    public function store()
    {  
        $rules = [
            'Titulo' => ['required', 'max:255'],
            'Descripcion' => ['required', 'max:1000'],
            'Precio' => ['required', 'min:1'],
            'Cantidad' => ['required', 'min:0'],
            'Estado' => ['required', 'in:Disponible,No disponible'],
        ];
            request()->validate($rules);

        if (request()->Estado == 'Disponible' && request()->Cantidad == 0) {
           
            return redirect()
            ->back()
            ->withInput(request()->all())
            ->withErrors('Si el producto esta disponible debe haber una cantidad');
           
        }
            $producto = Producto::create(request()->all());

            return redirect()
            ->route('productos.index')
            ->withSuccess("El nuevo producto con ID {$producto->id} ha sido creado");
        // ->with(['success' => "The new product with id {$product->id} was created"]);
    }

    public function show($producto)
    {
        $producto=Producto::findOrFail($producto);
        return view('producto.show')->with([
              'producto'=>$producto,
        ]);
    }

    public function edit($producto)
    {
        return view('producto.edit')->with([
            'producto' => Producto::findOrFail($producto),
        ]);
    }

    public function update($producto)
    {
        $rules = [
            'Titulo' => ['required', 'max:255'],
            'Descripcion' => ['required', 'max:1000'],
            'Precio' => ['required', 'min:1'],
            'Cantidad' => ['required', 'min:0'],
            'Estado' => ['required', 'in:Disponible,No disponible'],
        ];
            request()->validate($rules);
            
        $producto = Producto::findOrFail($producto);
        $producto->update(request()->all());
       
        return redirect()
        ->route('productos.index')
        ->withSuccess("ELl producto con ID {$producto->id} ha sido editado");
    }

    public function destroy($producto)
    {
        $producto = Producto::findOrFail($producto);

        $producto->delete();

        return redirect()
            ->route('productos.index')
            ->withSuccess("El producto con ID {$producto->id} ha sido eliminado");
    }
}