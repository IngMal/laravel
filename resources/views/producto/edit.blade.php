@extends('layouts.app')

@section('content')
    <h1>Edtit a product</h1>

    <form method="POST" action="{{ route('productos.update', ['producto' => $producto->id]) }}"> 
        @csrf
        @method('PUT') 
        <div class="form-row">
            <label>Titulo</label>
            <input class="form-control" type="text" name="Titulo" value="{{ old('Titulo') ?? $producto->Titulo }}">
        </div>
        <div class="form-row">
            <label>Descripcion</label>
            <input class="form-control" type="text" name="Descripcion" value="{{ old('Descripcion') ?? $producto->Descripcion }}">
        </div>
        <div class="form-row">
            <label>Precio</label>
            <input class="form-control" type="number" min="1.00" step="0.01" name="Precio" value="{{ old('Precio') ?? $producto->Precio }}">
        </div>
        <div class="form-row">
            <label>Cantidad</label>
            <input class="form-control" type="number" min="0" name="Cantidad" value="{{ old('Cantidad') ?? $producto->Cantidad }}">
        </div>
        <div class="form-row">
            <label>Estado</label>
            <select class="custom-select" name="Estado">

                <option {{ old('Estado') == 'Disponible' ? 'selected' : ($producto->Estado == 'Disponible' ? 'selected' : '') }} value="Disponible">
                    Disponible
                </option>

                <option {{ old('Estado') == 'No disponible' ? 'selected' : ($producto->Estado == 'No disponible' ? 'selected' : '') }} value="No disponible">
                    No disponible
                </option>

            </select>
        </div>
        <div class="form-row mt-3">
            <button type="submit" class="btn btn-primary btn-lg">Editar Producto</button>
        </div>
    </form>
@endsection